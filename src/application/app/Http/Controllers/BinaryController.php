<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Redis;


class BinaryController extends Controller
{

    public function index($number){

        $generatedArray = [];
        for ($i = 0; $i < 100; $i++) {
            $generatedArray[$i] = rand(0, 100);
        }

        if (Cache::get('generatedArray') == null){
            Cache::set('generatedArray', $generatedArray);
        }

        $cachedArray = Cache::get('generatedArray');
        sort($cachedArray);

/*        for ($i = 0; $i < count($cachedArray); $i++){
            echo $cachedArray[$i] . "\n";
        }*/

        $result = $this->binaryFindNum($number, $cachedArray);

        return $result;

    }

    private function binaryFindNum($number, $arr){

        sort($arr);

        $l = 0;
        $r = count($arr);
        $mid = $r / 2;
        for ($i = 0; $i < count($arr); $i++) {
            if ($number == $arr[$mid]) {
                return $mid;
                break;
            }
            if ($l == $mid || $r == $mid) {
                return -1;
                break;
            }
            if ($number < $arr[$mid]) {
                $r = $mid;
                $mid = round(($r - $l) / 2) + $l;
                continue;
            }
            if ($number > $arr[$mid]) {
                $l = $mid;
                $mid = round(($r - $l) / 2) + $l;
                continue;
            }
        }
    }

}
