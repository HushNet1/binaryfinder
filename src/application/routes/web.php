<?php

use App\Http\Controllers\BinaryController;
use Illuminate\Support\Facades\Route;

Route::get('/{number}', [BinaryController::class, 'index']);
